// External dependencies
import { geoPath } from 'd3-geo';
import { select } from 'd3-selection';
import { Feature, FeatureCollection } from 'geojson';
import { feature } from 'topojson-client';
import { Topology, GeometryCollection } from 'topojson-specification';

// Project asset dependencies
import countiesTopoJSON from './assets/counties.json';
import countiesEducationData from './assets/education.json';

type CountyEducationData = {
	'fips': number;
	'state': string;
	'area_name': string;
	'bachelorsOrHigher': number;
};

interface FeatureWithId extends Feature {
	id: string | number
};

interface CountiesFeatureCollection extends FeatureCollection {
	features: Array<FeatureWithId>;
};

// Convert TopoJSON GeometryCollection to GeoJSON FeatureCollection.
const counties = feature(
	/* For some reason TypeScript does not recognize countiesTopoJSON.type
	as being compatible with the "type" property of the Topology interface,
	even though they have the same string value.
	Because of this weird issue, I have to use "as unknown" before "as
	Topology". */
	// See also: https://mariusschulz.com/blog/string-literal-types-in-typescript#string-literal-types-vs-strings
	// See also: https://github.com/Microsoft/TypeScript/issues/28067
	countiesTopoJSON as unknown as Topology,
	countiesTopoJSON.objects.counties as GeometryCollection
) as CountiesFeatureCollection;

const height = 600;
const width = 1000;

let mouseX: number, mouseY: number;

document.addEventListener(
	'mousemove',
	( { pageX, pageY } ) => {
		mouseX = pageX;
		mouseY = pageY;
	}
);

const root = select( '#app-root' );

// Append title to root element.
root.append( 'h1' )
	.attr( 'id', 'title' )
	.text( 'United States Educational Attainment' );

// Append description to root element.
root.append( 'p' )
	.attr( 'id', 'description' )
	.html(
		'Percentage of adults age 25 and older with a bachelor\'s degree or higher (2010-2014).<br />' +
		'Source: <a rel="external" href="https://www.ers.usda.gov/data-products/county-level-data-sets/download-data.aspx">USDA Economic Research Service</a>'
	);

const lessThan10BachelorsColor = '#bfb';
const lessThan20BachelorsColor = '#9f9';
const lessThan30BachelorsColor = '#4e4';
const lessThan40BachelorsColor = '#0c0';
const atLeast40BachelorsColor = '#0a0';

// Append svg to root element.
const svg = root.append( 'svg' )
	.attr( 'class', 'choropleth-map' )
	.attr( 'viewBox', `0 0 ${ width } ${ height }` );

svg.selectAll( '.county' )
	.data( counties.features )
	.enter()
	.append( 'path' )
	.attr( 'class', 'county' )
	.attr( 'd', geoPath() ) // Use geoPath path generator.
	.attr( 'data-fips', d => d.id )
	.attr(
		'data-state',
		d => {
			// Find education data for county.
			const countyEduData: CountyEducationData | undefined = countiesEducationData.find( el => el.fips === d.id );

			if ( countyEduData !== undefined ) {
				return countyEduData.state;
			} else {
				throw new Error( 'countyEduData is undefined' );
			}
		}
	)
	.attr(
		'data-name',
		d => {
			// Find education data for county.
			const countyEduData: CountyEducationData | undefined = countiesEducationData.find( el => el.fips === d.id );

			if ( countyEduData !== undefined ) {
				return countyEduData.area_name;
			} else {
				throw new Error( 'countyEduData is undefined' );
			}
		}
	)
	.attr(
		'data-education',
		d => {
			// Find education data for county.
			const countyEduData: CountyEducationData | undefined = countiesEducationData.find( el => el.fips === d.id );

			if ( countyEduData !== undefined ) {
				return countyEduData.bachelorsOrHigher;
			} else {
				throw new Error( 'countyEduData is undefined' );
			}
		}
	)
	// Would have used classes, but inline styles are required for freeCodeCamp challenge tests.
	.attr(
		'fill',
		d => {
			// Find education data for county.
			const countyEduData: CountyEducationData | undefined = countiesEducationData.find( el => el.fips === d.id );

			if ( countyEduData !== undefined ) {
				if ( countyEduData.bachelorsOrHigher < 10 ) {
					return lessThan10BachelorsColor;
				} else if ( countyEduData.bachelorsOrHigher < 20 ) {
					return lessThan20BachelorsColor;
				} else if ( countyEduData.bachelorsOrHigher < 30 ) {
					return lessThan30BachelorsColor;
				} else if ( countyEduData.bachelorsOrHigher < 40 ) {
					return lessThan40BachelorsColor;
				} else {
					return atLeast40BachelorsColor;
				}
			} else {
				throw new Error( 'countyEduData is undefined' );
			}
		}
	)
	.on( 'mousemove', function() {
		const bachelorsOrHigher = this.getAttribute( 'data-education' );
		const state = this.getAttribute( 'data-state' );
		const name = this.getAttribute( 'data-name' );

		select( '#tooltip' )
			.attr(
				'data-education',
				() => bachelorsOrHigher
			)
			.style( 'display', 'block' )
			.style( 'position', 'absolute' )
			.style( 'left', `${ mouseX + 12 }px` )
			.style( 'top', `${ mouseY - 60 }px` )
			.html(
				`${ name }, ${ state }<br />` +
				`${ bachelorsOrHigher }%`
			);
	} )
	.on( 'mouseleave', function() {
		select( '#tooltip' )
			.style( 'display', 'none' );
	} );
	

const legend = svg.append( 'g' )
	.attr( 'id', 'legend' )
	.attr( 'transform', 'translate(240, 0)' );

legend.append( 'rect' )
	.attr( 'width', 375 )
	.attr( 'height', 24 )
	.style( 'fill', '#111' );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 8 )
	.attr( 'y', 12 )
	.attr( 'font-weight', '600' )
	.text( 'Percentage with bachelor’s degree or higher' );

// < 10 bachelors color example
legend.append( 'rect' )
	.attr( 'x', 170 )
	.attr( 'y', 8 )
	.attr( 'width', 8 )
	.attr( 'height', 8 )
	.attr( 'fill', lessThan10BachelorsColor );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 180 )
	.attr( 'y', 13 )
	.text( '< 10%' );

// < 20 bachelors color example
legend.append( 'rect' )
	.attr( 'x', 210 )
	.attr( 'y', 8 )
	.attr( 'width', 8 )
	.attr( 'height', 8 )
	.attr( 'fill', lessThan20BachelorsColor );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 220 )
	.attr( 'y', 13 )
	.text( '< 20%' );

// < 30 bachelors color example
legend.append( 'rect' )
	.attr( 'x', 250 )
	.attr( 'y', 8 )
	.attr( 'width', 8 )
	.attr( 'height', 8 )
	.attr( 'fill', lessThan30BachelorsColor );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 260 )
	.attr( 'y', 13 )
	.text( '< 30%' );

// < 40 bachelors color example
legend.append( 'rect' )
	.attr( 'x', 290 )
	.attr( 'y', 8 )
	.attr( 'width', 8 )
	.attr( 'height', 8 )
	.attr( 'fill', lessThan40BachelorsColor );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 300 )
	.attr( 'y', 13 )
	.text( '< 40%' );

// >= 40 bachelors color example
legend.append( 'rect' )
	.attr( 'x', 330 )
	.attr( 'y', 8 )
	.attr( 'width', 8 )
	.attr( 'height', 8 )
	.attr( 'fill', atLeast40BachelorsColor );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 340 )
	.attr( 'y', 13 )
	.text( '>= 40%' );

root.append( 'p' )
	.attr( 'id', 'tooltip' )
	.style( 'display', 'none' );